{-# LANGUAGE PackageImports #-}
import "UnipolyAchat" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
