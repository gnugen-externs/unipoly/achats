module Foundation where

import Import.NoFoundation
import Database.Persist.Sql (ConnectionPool, runSqlPool)
import Text.Hamlet          (hamletFile)
import Text.Jasmine         (minifym)
import Yesod.Default.Util   (addStaticContentExternal)
import Yesod.Core.Types     (Logger)
import qualified Yesod.Core.Unsafe as Unsafe

-- Useful
import Data.Text (strip)
import Yesod.Auth.LDAP
import LDAP.Init (ldapInitialize, ldapSimpleBind)
import LDAP.Modify
import LDAP.Exceptions (catchLDAP, throwLDAP, LDAPException(..))
import LDAP.Data (LDAPReturnCode(..))
import LDAP.Types (LDAP)
import LDAP.Search (LDAPScope(..))

-- | The foundation datatype for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { appSettings    :: AppSettings
    , appStatic      :: Static -- ^ Settings for static file serving.
    , appConnPool    :: ConnectionPool -- ^ Database connection pool.
    , appHttpManager :: Manager
    , appLogger      :: Logger
    }

instance HasHttpManager App where
    getHttpManager = appHttpManager

-- Set up i18n messages. See the message folder.
mkMessage "App" "messages" "fr"

-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- http://www.yesodweb.com/book/routing-and-handlers
--
-- Note that this is really half the story; in Application.hs, mkYesodDispatch
-- generates the rest of the code. Please see the linked documentation for an
-- explanation for this split.
--
-- This function also generates the following type synonyms:
-- type Handler = HandlerT App IO
-- type Widget = WidgetT App IO ()
mkYesodData "App" $(parseRoutesFile "config/routes")

instance RenderMessage App Bool where
    renderMessage master langs t = renderMessage master langs (conv t)
        where
            conv True  = MsgYes
            conv False = MsgNo

instance RenderMessage App SitePage where
    renderMessage master langs t = renderMessage master langs (conv t)
        where
            conv  PageCommand   =  MsgPageCommand
            conv  PageSummary   =  MsgPageSummary
            conv  PageInfo      =  MsgPageInfo
            conv  PageLink      =  MsgPageLink
            conv  PageUser      =  MsgPageUser
            conv  PageManage    =  MsgPageManage
            conv  PageProduct   =  MsgPageProduct
            conv  PageAnnounce  =  MsgPageAnnounce
            conv  PageSupplier  =  MsgPageSupplier

-- | A convenient synonym for creating forms.
type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.
instance Yesod App where
    -- Controls the base of generated URLs. For more information on modifying,
    -- see: https://github.com/yesodweb/yesod/wiki/Overriding-approot
    approot = ApprootMaster $ appRoot . appSettings

    -- Store session data on the client in encrypted cookies,
    -- default session idle timeout is 120 minutes
    makeSessionBackend _ = Just <$> defaultClientSessionBackend
        120    -- timeout in minutes
        "config/client_session_key.aes"


    -- Max size of file upload
    maximumContentLength _ _ = Just $ 25 * 1024 * 1024

    defaultLayout widget = do
        master <- getYesod
        mmsg <- getMessage
        mUser <- maybeAuth
        uIsAdmin <- isAdmin
        let mUsername = userName . entityVal <$> mUser

        -- We break up the default layout into two components:
        -- default-layout is the contents of the body tag, and
        -- default-layout-wrapper is the entire page. Since the final
        -- value passed to hamletToRepHtml cannot be a widget, this allows
        -- you to use normal widget features in default-layout.

        pc <- widgetToPageContent $ do
            {-$(combineStylesheets 'StaticR
                [ css_normalize_css
                , css_bootstrap_css
                , css_style_css
                ])-}
            $(widgetFile "default-layout")
        withUrlRenderer $(hamletFile "templates/default-layout-wrapper.hamlet")

    -- The page to be redirected to when authentication is required.
    authRoute _ = Just $ AuthR LoginR

    -- Authorizations
    isAuthorized FaviconR                 _ = return Authorized
    isAuthorized RobotsR                  _ = return Authorized
    isAuthorized (AuthR _)                _ = return Authorized
    isAuthorized (StaticR _)              _ = return Authorized

    isAuthorized HomeR                    _ = isLoggedAuth
    isAuthorized SummaryR                 _ = isLoggedAuth
    isAuthorized DocumentR                _ = isLoggedAuth
    isAuthorized (DocumentGetR _)         _ = isLoggedAuth
    isAuthorized (CommandLineDelete _)    _ = isLoggedAuth
    isAuthorized UserR                    _ = isLoggedAuth
    isAuthorized LinkR                    _ = isLoggedAuth

    isAuthorized _                        _ = isAdminAuth

    -- This function creates static content files in the static folder
    -- and names them based on a hash of their content. This allows
    -- expiration dates to be set far in the future without worry of
    -- users receiving stale content.
    addStaticContent ext mime content = do
        master <- getYesod
        let staticDir = appStaticDir $ appSettings master
        addStaticContentExternal
            minifym
            genFileName
            staticDir
            (StaticR . flip StaticRoute [])
            ext
            mime
            content
      where
        -- Generate a unique filename based on the content itself
        genFileName lbs = "autogen-" ++ base64md5 lbs

    -- What messages should be logged. The following includes all messages when
    -- in development, and warnings and errors in production.
    shouldLog app _source level =
        appShouldLogAll (appSettings app)
            || level == LevelWarn
            || level == LevelError

    makeLogger = return . appLogger

isAdmin :: Handler Bool
isAdmin = maybeAuth >>= return . maybe False (userIsAdmin . entityVal)

isAdminAuth :: Handler AuthResult
isAdminAuth = do
  mu <- maybeAuth
  return $ case userIsAdmin . entityVal <$> mu of
    Nothing -> AuthenticationRequired
    Just True -> Authorized
    Just False -> Unauthorized "You must be an admin"

isLoggedAuth :: Handler AuthResult
isLoggedAuth = do
  mu <- maybeAuth
  return $ case mu of
    Just (Entity _ u) -> case userIsLocked u of
        True  -> Unauthorized "Your account is disabled, contact admin"
        False -> Authorized
    Nothing -> AuthenticationRequired

-- How to run database actions.
instance YesodPersist App where
    type YesodPersistBackend App = SqlBackend
    runDB action = do
        master <- getYesod
        runSqlPool action $ appConnPool master
instance YesodPersistRunner App where
    getDBRunner = defaultGetDBRunner appConnPool

instance YesodAuthLdap App where
    getLdapConfig = do
        extra <- getExtra
        return LDAPConfig
            { usernameFilter = \n -> "mail=" <> n
            , identifierModifier = \n _ -> n
            , ldapUri = unpack $ appLdapUri extra
            , baseDN = Just $ unpack $ appLdapDN extra
            , initDN = unpack $ appLdapAuthDN extra
            , initPass = unpack $ appLdapAuthPass extra
            , ldapScope = LdapScopeSubtree
            }

instance YesodAuth App where
    type AuthId App = UserId

    -- Where to send a user after successful login
    loginDest _ = HomeR
    -- Where to send a user after logout
    logoutDest _ = HomeR
    -- Override the above two destinations when a Referer: header is present
    redirectToReferer _ = True

    -- Custom display for login
    loginHandler = do
        tp <- getRouteToParent
        lift $ defaultLayout $ do
            master <- getYesod
            let wLdap = apLogin authLDAP tp
            $(widgetFile "login")


    authenticate (Creds "LDAP" ident extra) = runDB $ do
        x <- getBy $ UniqueUser $ strip ident

        let fullname = fromMaybe "(error)" $ lookup "cn" extra
            ldapBool "TRUE" = True
            ldapBool _ = False
            uAdmin = fromMaybe False $ ldapBool <$>
                lookup "unipolyAchatsAdmin" extra
            uEnable = fromMaybe False $ ldapBool <$>
                lookup "unipolyAchatsEnable" extra

        uid <- case x of
            Just (Entity uid _) ->
                return uid
            Nothing -> do
                insert $ (userDefault ident)
                    { userName = fullname
                    , userVerified = True
                    , userIsAdmin = uAdmin
                    , userIsLocked = not uEnable
                    }

        return $ Authenticated uid

    authenticate _ = return $ ServerError "Unsupported auth method"

    -- You can add other plugins like BrowserID, email or OAuth here
    authPlugins _ = [authLDAP]

    authHttpManager = getHttpManager

instance YesodAuthPersist App

userDefault :: Text -> User
userDefault ident = User
    { userIdent = ident
    , userPassword = Nothing
    , userVerkey = Nothing
    , userVerified = False
    , userName = "(error)"
    , userEmail = Nothing
    , userPhone = Nothing
    , userIsAdmin = False
    , userIsLocked = False
    , userHasCarPermit = False
    }

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.
instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

unsafeHandler :: App -> Handler a -> IO a
unsafeHandler = Unsafe.fakeHandlerGetLogger appLogger

-- | Get the 'Extra' value, used to hold data from the settings.yml file.
getExtra :: Handler AppSettings
getExtra = appSettings <$> getYesod

{- Diverse small helpers specific to our project -}
rangeField :: Double -> Double -> Double -> Field Handler Double
rangeField u v step = checkBool checks MsgInvalidQuantity doubleField'
    where
        checks = \x -> (inRange u v x) && (floatDiv x step)
        doubleField' = doubleField
            { fieldView = \theId name attrs val isReq -> toWidget [hamlet|
                $newline never
                <input id="#{theId}" name="#{name}" *{attrs} type="number" step=#{show step} :isReq:required="" value="#{showVal val}">
                    |]
            }
        showVal = either id (pack . showNum)

intDblField :: Field Handler Double -> Field Handler Double
intDblField f = checkBool (\x -> x == fromInteger (round x)) MsgNonIntegerQuantity f

announceWidget :: SitePage -> Widget
announceWidget page = do
    anns <- handlerToWidget $
        runDB $ selectList [AnnouncePage ==. page, AnnounceShown ==. True]
                           [Desc AnnounceTime]
    $(widgetFile "widget_announce")

selectBoolField :: Field Handler Bool
selectBoolField = selectFieldList [(MsgYes, True), (MsgNo, False)]

getLdapConnection :: Handler LDAP
getLdapConnection = do
    -- Custom: LDAP persistent connection
    extra <- getExtra
    let ldapUri = unpack $ appLdapUri extra
        ldapAuthDN = unpack $ appLdapAuthDN extra
        ldapAuthPass = unpack $ appLdapAuthPass extra
    ldap <- liftIO $ ldapInitialize ldapUri
    liftIO $ ldapSimpleBind ldap ldapAuthDN ldapAuthPass
    return ldap

updateUserLdap :: User -> Handler ()
updateUserLdap u = do
    extra <- getExtra
    ldap <- getLdapConnection
    -- sync achats attrs in the LDAP
    let dn = unpack $ "mail=" <> userIdent u <> "," <> appLdapDN extra
        ldapBool True = "TRUE"
        ldapBool False = "FALSE"
        mods = [ ("unipolyAchatsAdmin", userIsAdmin u)
               , ("unipolyAchatsEnable", not $ userIsLocked u)]
        ldapMods = for mods $
            \(attr, b) -> LDAPMod LdapModReplace attr [ldapBool b]

    liftIO $ catchLDAP (ldapModify ldap dn ldapMods) $ \e ->
        case (code e) of
             LdapNoSuchObject ->
                 putStrLn $ "updateUserLdap: Skip LDAP user missing "
                             <> textShow dn
             _ -> throwLDAP e

    -- add/rem from achats groups if enabled/disabled
    let groupdn = "cn=achats,ou=Groups,dc=unipoly,dc=epfl,dc=ch"
        groupOp = if userIsLocked u then LdapModDelete else LdapModAdd
        groupMods = [ LDAPMod groupOp "uniqueMember" [dn] ]

    liftIO $ catchLDAP (ldapModify ldap groupdn groupMods) $ \e ->
        case (code e) of
             LdapTypeOrValueExists ->
                 putStrLn $ "updateUserLdap: Skip LDAP already set "
                            <> textShow dn
             LdapNoSuchAttribute ->
                 putStrLn $ "updateUserLdap: Skip LDAP already set "
                            <> textShow dn
             _ -> throwLDAP e

