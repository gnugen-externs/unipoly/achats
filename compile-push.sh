#!/bin/sh
# compile project into debian stable chroot
set -ex
cabal clean
cabal build -j
BINARY=./dist/build/UnipolyAchat/UnipolyAchat
strip "$BINARY"
rsync --progress --inplace -zv "$BINARY" morgarten:./
