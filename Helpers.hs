{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Helpers where

import ClassyPrelude hiding (for)
import qualified Prelude as Unsafe (head)
import Text.Printf
import Yesod
import qualified Database.Esqueleto as E
import Data.Time.LocalTime (TimeOfDay(..), timeToTimeOfDay)
import qualified Data.Set as S
import Data.Fixed (mod')

{- Helpers -}
for :: [a] -> (a -> b) -> [b]
for = flip map

keyToInt :: PersistEntity a => Key a -> Int64
keyToInt eId = case (keyToValues eId) of
                    (PersistInt64 v) : [] -> v
                    _ -> error "Expected a integer key"

showNum :: (Num a, Show a) => a -> String
showNum = show

showPrice :: Double -> Text
showPrice p = pack $ printf "%.2f" roundedp
    where roundedp :: Double
          roundedp = (fromIntegral $ (round $ p * 20 :: Integer)) / 20
          -- ^ round prices for CHF (at the nearest 0.05, thus 1/0.05 = 20)

showQty :: Double -> String
showQty q = printf "%.2f" q

textShow :: Show a => a -> Text
textShow = pack . show

unValue :: E.Value a -> a
unValue (E.Value a) = a

showTime :: UTCTime -> Text
showTime t = pack $ printf "%02i/%02i/%i %02ih%02i" day month year hour mn
    where
        (year, month, day) = toGregorian $ utctDay t
        TimeOfDay hour mn _ = timeToTimeOfDay $ utctDayTime t

inRange :: Ord a => a -> a -> a -> Bool
inRange min' max' x = min' <= x && x <= max'

floatDiv :: (Fractional a, Real a) => a -> a -> Bool
floatDiv x y = (x `mod'` y) < 1/1000

bucketOn :: Ord b => (a -> b) -> [a] -> [(b, [a])]
bucketOn zoom' xs = map (fst . Unsafe.head &&& map snd) $
        groupBy (\x y -> fst x == fst y) $
        sortOn fst $
        map (zoom' &&& id) xs

nub' :: (Ord a) => [a] -> [a]
nub' = go S.empty
  where go _ [] = []
        go s (x:xs) | S.member x s = go s xs
                    | otherwise    = x : go (S.insert x s) xs
