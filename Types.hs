{-# LANGUAGE TemplateHaskell #-}
module Types where

import Prelude
import Data.Data
import Yesod


data SitePage = PageCommand | PageSummary | PageInfo | PageLink | PageUser
              | PageManage | PageProduct | PageAnnounce | PageSupplier
    deriving (Eq, Read, Show, Typeable, Enum, Bounded)
derivePersistField "SitePage"

data SettingName = CommandStatus
    deriving (Eq, Read, Show, Typeable, Enum, Bounded)
derivePersistField "SettingName"
