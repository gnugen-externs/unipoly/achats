Unipoly-Achat README
====================

**NOTE** This README assumes you are running Debian Testing (Jessie).

You will need to have the following packages installed:

* libldap-dev
* haskell-platform

In general, for the basic setup, please refer to
http://www.yesodweb.com/page/quickstart. It is recommended to initialise a
sandbox (we use one in the following steps); while not mandatory, this will
avoid what is called "cabal hell").

Next you will need to add the required sub-repositories `authenticate-ldap` and
`yesod-auth-ldap` (because they are unmaintained upstream):

	git submodule init
	git submodule update

The cabal sandbox is created and will also need to be told to use the code
contained therein:

    cabal sandbox init
	cabal sandbox add-path deps/yesod-auth-ldap
	cabal sandbox add-path deps/authenticate-ldap

Then, install the needed packages to run the webapp:

	cabal update
	cabal install -j happy alex yesod-bin
    cabal install -j --only-dep

Finally, you should be able to run the server (found in .cabal-sandbox/bin):

	.cabal-sandbox/bin/yesod devel

To compile the final binary (see compile-push.sh):

    cabal build -j


troubleshooting
---------------

### avoid redirection to `localhost`

In case your webapp is not running on `localhost` (e.g. you're running it inside
a VM), you will need to set the APPROOT environment variable, by default:
    APPROOT="http://localhost:3000"

This is usually achieved by setting your machine's IP (**NOTE** setting the
machine name will usually not work, since it will be resolved to `localhost`
locally, before being sent to the client).
