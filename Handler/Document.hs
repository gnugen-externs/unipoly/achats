{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Document where

import Import
import qualified Data.Text as T (concat)
import qualified Data.ByteString as S
import qualified Data.ByteString.Lazy as L
import Data.Conduit.Binary (sinkLbs)


getDocumentR :: Handler Html
getDocumentR = do
    ds <- runDB $ selectList ([]::[Filter Document]) []
    uIsAdmin <- isAdmin

    defaultLayout $(widgetFile "document")


fileFieldData :: FileInfo -> Handler ByteString
fileFieldData file = do
    fileBytes <- runResourceT $ fileSource file $$ sinkLbs
    return $ S.pack . L.unpack $ fileBytes


handleDocumentAddR :: Handler Html
handleDocumentAddR = do
    uid <- requireAuthId
    now <- liftIO getCurrentTime

    ((fRes, fWidget), fEnc) <- runFormPost . renderDivs $ (\x y z a -> (x, y, z, a))
        <$> areq textField "Nom" Nothing
        <*> areq textField "Description" Nothing
        <*> areq fileField "Fichier" Nothing
        <*> areq selectBoolField "Pour admin" Nothing


    case fRes of
         FormSuccess (name, desc, file, onlyAdmin) -> do
             fData <- fileFieldData file

             let fname = fileName file
                 ftype = fileContentType file

             _ <- runDB $ insert $
                 Document uid name desc fname ftype fData now onlyAdmin
             setMessageI MsgDocumentAdded
             redirect DocumentR

         _ -> return ()

    defaultLayout $ [whamlet|
        <h1>Ajouter un document

        <form method=post enctype=#{fEnc}>
          ^{fWidget}
          <button type=submit .pull-right .btn .btn-primary>
            Ajouter le document
    |]


handleDocumentEditR :: DocumentId -> Handler Html
handleDocumentEditR did = do
    now <- liftIO getCurrentTime
    d <- runDB $ get404 did

    ((fRes, fWidget), fEnc) <- runFormPost . renderDivs $ (\x y z a -> (x, y, z, a))
        <$> areq textField "Nom" (Just $ documentName d)
        <*> areq textField "Description" (Just $ documentDesc d)
        <*> aopt fileField "Fichier" Nothing
        <*> areq selectBoolField "Pour admin" (Just $ documentOnlyAdmin d)


    case fRes of
         FormSuccess (name, desc, mFile, onlyAdmin) -> do
             d2 <- case mFile of
                  Just file -> do
                     fData <- fileFieldData file
                     let fname = fileName file
                         ftype = fileContentType file

                     return $ d { documentFilename = fname
                                , documentType = ftype
                                , documentContent = fData }
                  Nothing ->
                      return d

             let d3 = d2 { documentName = name
                         , documentDesc = desc
                         , documentTime = now
                         , documentOnlyAdmin = onlyAdmin }

             runDB $ replace did d3
             setMessageI MsgDocumentEdited
             redirect DocumentR

         _ -> return ()

    defaultLayout $ [whamlet|
        <h1>Éditer un document

        <form method=post enctype=#{fEnc}>
          ^{fWidget}
          <button type=submit .pull-right .btn .btn-primary>
            Éditer le document
          <a href=@{DocumentDeleteR did} .pull-right .btn .btn-danger>
            Supprimer
    |]


handleDocumentDeleteR :: DocumentId -> Handler Html
handleDocumentDeleteR did = do
    _ <- runDB $ get404 did
    runDB $ delete did
    setMessageI MsgDocumentDeleted
    redirect DocumentR


getDocumentGetR :: DocumentId -> Handler TypedContent
getDocumentGetR did = do
    d <- runDB $ get404 did
    addHeader "Content-Disposition" $ T.concat
        [ "attachment; filename=\"", documentName d, "\""]
    sendResponse (encodeUtf8 $ documentType d, toContent $ documentContent d)
