{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Command where

import Import
import qualified Database.Esqueleto as E
import qualified Data.Csv as C
import qualified Data.ByteString.Lazy.Internal as LB (ByteString)

getCommandViewR :: Handler Html
getCommandViewR = do
    xs <- runDB $ E.select $ E.from $ \(c `E.InnerJoin` p `E.InnerJoin` u `E.InnerJoin` s) -> do
      E.on $ c E.^. CommandProduct  E.==. p E.^. ProductId
      E.on $ c E.^. CommandBuyer    E.==. u E.^. UserId
      E.on $ p E.^. ProductSupplier E.==. s E.^. SupplierId
      E.orderBy [E.asc $ u E.^. UserId, E.asc $ p E.^. ProductId]
      E.where_ $ E.isNothing $ c E.^. CommandClosedBy
      return (c, p, u, s)

    let xsPrices :: [(Entity User, Double)]
        xsPrices = for xs $ \(Entity _ c, _, u, _) ->
          (u, commandTotalPrice c)
        tPrice = sum $ map snd xsPrices

    let users = nub' $ map fst xsPrices
        uPrices = for users $
          \u -> (u, sum $ map snd $ filter ((==) u . fst) xsPrices)

    let ysQtyPxSup :: [(Entity Product, (Double, Double, Entity Supplier))]
        ysQtyPxSup = for xs $ \(Entity _ c, pe, _, s) ->
          (pe, (commandQuantity c, commandTotalPrice c, s))

    let prods = nub' $ map fst ysQtyPxSup
        pQtyPxSup :: [(Entity Product, (Double, Double, Entity Supplier))]
        pQtyPxSup = sortOn (productName . entityVal . fst) $ for prods $ \p ->
          let entries = map snd $ filter ((==) p . fst) ysQtyPxSup
              eFirst:_ = entries
              pPrice = sum $ map (\(_, price, _) -> price) entries
              pQty = sum $ map (\(qty, _, _) -> qty) entries
              pSup = (\(_, _, s) -> s) eFirst
          in (p, (pQty, pPrice, pSup))

    let suppliers = nub' $ map (\(_, _, _, s) -> s) xs :: [Entity Supplier]

    mCmdStatus <- runDB $ getBy $ UniqueSetting CommandStatus

    defaultLayout $ do
        $(widgetFile "command_view")

postCommandArchiveR :: Handler Html
postCommandArchiveR = do
  uId <- requireAuthId
  time <- liftIO $ getCurrentTime

  cmd <- runDB $ selectList [CommandClosedBy ==. Nothing] []
  let totalPrice = sum $ map (commandTotalPrice . entityVal) cmd
      cmdIds = map entityKey cmd

  clId <- runDB . insert $ Closed uId time totalPrice
  runDB $ updateWhere [CommandId <-. cmdIds] [CommandClosedBy =. Just clId]

  setMessageI MsgCommandArchived
  redirect CommandViewR

postCommandLockR :: Handler Html
postCommandLockR = do
  _ <- runDB $ do
    deleteBy $ UniqueSetting CommandStatus
  setMessageI MsgCommandLocked
  redirect CommandViewR

postCommandOpenR :: Handler Html
postCommandOpenR = do
  _ <- runDB $ do
    deleteBy $ UniqueSetting CommandStatus
    insertBy $ Setting CommandStatus ""
  setMessageI MsgCommandOpened
  redirect CommandViewR

getCommandOldR :: Handler Html
getCommandOldR = do
  cls <- runDB $ selectList [] [Desc ClosedTime]

  defaultLayout $ do
      $(widgetFile "command_old")

getCommandExportSupplierR :: Handler TypedContent
getCommandExportSupplierR = do
  let supplierOptions = optionsPersistKey [] [] (const (""::Text))
  msid <- runInputGet $ iopt (selectField supplierOptions) "supplier"

  commandExportSupplierHelper Nothing msid

getCommandExportSupplierOldR :: ClosedId -> Handler TypedContent
getCommandExportSupplierOldR clid =
  commandExportSupplierHelper (Just clid) Nothing

-- Export in CSV, open commands for the given supplier
commandExportSupplierHelper :: Maybe ClosedId -> Maybe SupplierId -> Handler TypedContent
commandExportSupplierHelper mClosedId mSupplierId = do
    xs <- runDB $ E.select $ E.from $ \(c `E.InnerJoin` p `E.InnerJoin` s) -> do
      E.on $ p E.^. ProductSupplier E.==. s E.^. SupplierId
      E.on $ c E.^. CommandProduct  E.==. p E.^. ProductId
      E.where_ $ case mClosedId of
          Just clid -> c E.^. CommandClosedBy E.==. E.just (E.val clid)
          Nothing -> E.isNothing $ c E.^. CommandClosedBy
      E.where_ $ case mSupplierId of
          Just sid -> s E.^. SupplierId E.==. E.val sid
          Nothing -> s E.^. SupplierDeleted E.==. E.val False
      E.orderBy [E.asc $ s E.^. SupplierName, E.asc $ p E.^. ProductName]
      E.groupBy (p E.^. ProductId, s E.^. SupplierId)
      return (p, s,
            E.sum_ $ c E.^. CommandQuantity,
            E.sum_ $ c E.^. CommandTotalPrice)

    let headers = [ "Fournisseur", "Produit", "Par unité", "Quantité", "Total" ]
        headersVec = fromList $ map encodeUtf8 headers
        rows = for xs $ \(Entity _ p, Entity _ s, qty, price) ->
          let mDbl = fromMaybe 0.0 . unValue
              qty' = mDbl qty
              price' = mDbl price
          in CsvRowSupplier s p qty' price'

    csv <- csvPrependDate mClosedId $ C.encodeByName headersVec rows
    return $ TypedContent typeCsv $ toContent csv

getCommandExportUserR :: Handler TypedContent
getCommandExportUserR = commandExportUserHelper Nothing

getCommandExportUserOldR :: ClosedId -> Handler TypedContent
getCommandExportUserOldR clid = commandExportUserHelper $ Just clid

-- Export in CSV, open commands per user
commandExportUserHelper :: Maybe ClosedId -> Handler TypedContent
commandExportUserHelper mClosedId = do
    xs <- runDB $ E.select $ E.from $ \(c `E.InnerJoin` p `E.InnerJoin` u) -> do
      E.on $ c E.^. CommandProduct  E.==. p E.^. ProductId
      E.on $ c E.^. CommandBuyer    E.==. u E.^. UserId
      E.where_ $ case mClosedId of
          Just clid -> c E.^. CommandClosedBy E.==. E.just (E.val clid)
          Nothing -> E.isNothing $ c E.^. CommandClosedBy
      E.orderBy [E.asc $ u E.^. UserName, E.asc $ p E.^. ProductName]
      E.groupBy (p E.^. ProductId, u E.^. UserId)
      return (p, u,
            E.sum_ $ c E.^. CommandQuantity,
            E.sum_ $ c E.^. CommandTotalPrice)
    let _ = xs :: [(Entity Product, Entity User, E.Value (Maybe Double), E.Value (Maybe Double))]

    let users = nub' $ map (\(_, u, _, _) -> u) xs
        prods = nub' $ map (\(p, _, _, _) -> p) xs
        userProds = for xs $ \(Entity pId _, Entity uId _, qty, price) ->
            let fn = fromMaybe 0.0 . unValue
            in ((pId, uId), (fn qty, fn price))

    let headers = "Produits" : "Par unité" : map (userName . entityVal) users
        rows = for prods $ \(Entity pId p) ->
            let row = for users $ \(Entity uId _) ->
                    maybe "" (pack . showQty . fst) $
                        lookup (pId, uId) userProds
                priceFmt = showProductUnitPrice p
            in (productName p) : priceFmt : row
        total = "Total CHF" : "" : (for users $ \(Entity uId _) ->
            showPrice $ sum $ for prods $ \(Entity pId _) ->
                maybe 0.0 (snd) $ lookup (pId, uId) userProds)

    csv <- csvPrependDate mClosedId $ C.encode (headers : rows ++ [[], total])
    return $ TypedContent typeCsv $ toContent csv

getCommandExportAccountR :: Handler TypedContent
getCommandExportAccountR = commandExportAccountHelper Nothing

getCommandExportAccountOldR :: ClosedId -> Handler TypedContent
getCommandExportAccountOldR clid = commandExportAccountHelper $ Just clid

-- Export in CSV, total price of open commands per user
commandExportAccountHelper :: Maybe ClosedId -> Handler TypedContent
commandExportAccountHelper mClosedId = do
    now <- liftIO getCurrentTime

    xs <- map (\(x,y,z) -> (x, fromMaybe 0.0 (unValue y), fromMaybe now (unValue z))) <$>
      (runDB $ E.select $ E.from $ \(u `E.LeftOuterJoin` c) -> do
        E.on $ (u E.^. UserId E.==. c E.^. CommandBuyer)
            E.&&. (case mClosedId of
              Just clid -> c E.^. CommandClosedBy E.==. E.just (E.val clid)
              Nothing -> E.isNothing $ c E.^. CommandClosedBy)
        E.where_ $ u E.^. UserIsLocked E.==. E.val False
        E.orderBy [E.asc $ u E.^. UserName]
        E.groupBy (u E.^. UserId)
        return (u, E.sum_ $ c E.^. CommandTotalPrice, E.max_ $ c E.^. CommandTime)
      )
    let _ = xs :: [(Entity User, Double, UTCTime)]

    let headers = ["Utilisateur", "Montant", "Date"]
        headersVec = fromList $ map encodeUtf8 headers
        totalSum = sum $ for xs $ \(_,y,_) -> y
        rows = (for xs $ \(Entity _ u, price, date) ->
                CsvRowAccount (userName u) price date
               ) ++ [CsvRowAccount "Total CHF" totalSum now]

    csv <- csvPrependDate mClosedId $ C.encodeByName headersVec rows
    return $ TypedContent typeCsv $ toContent csv


csvPrependDate :: Maybe ClosedId -> LB.ByteString -> Handler LB.ByteString
csvPrependDate mClosedId csv = do
    date <- case mClosedId of
        Nothing -> liftIO getCurrentTime
        Just clid -> closedTime <$> runDB (get404 clid)

    let dateRow = ["Date: " <> showTime date]
    return $ C.encode [dateRow] <> csv


data CsvRowSupplier = CsvRowSupplier
  { csvSupplier   :: !Supplier
  , csvProduct    :: !Product
  , csvQuantity   :: !Double
  , csvTotalPrice :: !Double
  }

instance C.ToNamedRecord CsvRowSupplier where
  toNamedRecord (CsvRowSupplier s p qty tprice)  = C.namedRecord
    [ encodeUtf8 "Fournisseur" C..= (supplierName s)
    , encodeUtf8 "Produit"     C..= (productName p)
    , encodeUtf8 "Par unité"   C..= (showProductUnitPrice p)
    , encodeUtf8 "Quantité"    C..= (showQty qty)
    , encodeUtf8 "Total"       C..= (showPrice tprice)
    ]

data CsvRowAccount = CsvRowAccount
  { csvUser   :: !Text
  , csvTotal  :: !Double
  , csvDate   :: !UTCTime
  }

instance C.ToNamedRecord CsvRowAccount where
  toNamedRecord row = C.namedRecord
    [ encodeUtf8 "Utilisateur" C..= csvUser row
    , encodeUtf8 "Montant"     C..= showPrice (csvTotal row)
    , encodeUtf8 "Date"        C..= showTime (csvDate row)
    ]

typeCsv :: ContentType
typeCsv = "text/csv; charset=utf-8"

showProductUnitPrice :: Product -> Text
showProductUnitPrice p = (showPrice $ productPrice p) <> " CHF/"
                        <> (productUnit p)
