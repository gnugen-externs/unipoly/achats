{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Product where

import Import
import qualified Database.Esqueleto as E
import qualified Prelude as Unsafe (head)

getProductR :: Handler Html
getProductR = do
  xs <- runDB $ E.select $ E.from $ \(p `E.InnerJoin` s) -> do
    E.on $ p E.^. ProductSupplier E.==. s E.^. SupplierId
    E.where_ $ p E.^. ProductDeleted E.==. E.val False
    E.orderBy [E.asc $ p E.^. ProductName]
    return (p, s)
  let _ = xs :: [(Entity Product, Entity Supplier)]

  let gfxs = (productCategory . entityVal . fst) `bucketOn` xs
      cats = map fst gfxs

  defaultLayout $(widgetFile "product")

handleProductEditR :: ProductId -> Handler Html
handleProductEditR pId = do
  prod <- runDB $ get404 pId

  ((fRes, fWidget), fEnc) <- runFormPost . renderDivs $ productForm (Just prod)

  case fRes of
    FormSuccess prod' -> do
      runDB $ replace pId prod'
      setMessageI MsgProductEdited
      redirect ProductR
    _ -> do
      return ()

  defaultLayout $ [whamlet|
    <h1>Éditer un produit

    <form method=post enctype=#{fEnc}>
        ^{fWidget}
        <button type=submit .pull-right .btn .btn-primary>
          Éditer le produit
        <a href=@{ProductDeleteR pId} .pull-right .btn .btn-danger>
          Supprimer
  |]

handleProductAddR :: Handler Html
handleProductAddR = do
  ((fRes, fWidget), fEnc) <- runFormPost . renderDivs $ productForm Nothing

  case fRes of
    FormSuccess prod' -> do
      runDB $ insert_ prod'
      setMessageI MsgProductAdded
      redirect ProductR
    _ -> do
      return ()

  defaultLayout $ [whamlet|
    <h1>Ajouter un produit

    <form method=post enctype=#{fEnc}>
        ^{fWidget}
        <button type=submit .pull-right .btn .btn-primary>
          Ajouter le produit
  |]


handleProductDeleteR :: ProductId -> Handler Html
handleProductDeleteR pid = do
  _ <- runDB $ get404 pid
  runDB $ updateWhere [ProductId ==. pid]
    [ProductDeleted =. True, ProductShown =. False]
  setMessageI MsgProductDeleted
  redirect ProductR


postProductSetShownR :: ProductId -> Bool -> Handler Html
postProductSetShownR pid v = do
    p <- runDB $ get404 pid
    runDB $ replace pid $ p { productShown = v }
    redirect ProductR


productForm :: Maybe Product -> AForm Handler Product
productForm prod =
  let supplierOptions = optionsPersistKey [] [Asc SupplierName] (toMessage . supplierName)

  in Product
    <$> areq textField "Nom" (productName <$> prod)
    <*> areq (selectField supplierOptions) "Fournisseur" (productSupplier <$> prod)
    <*> areq textField "Provenance" (productOrigin <$> prod)
    <*> areq selectBoolField "Visible" (productShown <$> prod)
    <*> areq doubleField "Prix unitaire" (productPrice <$> prod)
    <*> areq textField "Unité" (productUnit <$> prod)
    <*> areq textField "Catégorie" (productCategory <$> prod)
    <*> areq doubleField "Quantité max" (productMaxQuantity <$> prod)
    <*> areq doubleField "Incrément de quantité" (productStepQuantity <$> prod)
    <*> pure False
