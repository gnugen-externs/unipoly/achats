{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.User where

import Import

getUserR :: Handler Html
getUserR = do
  xs <- runDB $ selectList ([]::[Filter User]) []

  uIsAdmin <- isAdmin

  defaultLayout $ [whamlet|
    <h1>Liste des utilisateurs

    ^{announceWidget PageUser}

    <table .userTable>
      <thead>
        <tr>
          <th>Nom
          <th>Identifiant
          <th>Téléphone
          <th>Permis v.
          <th>
      <tbody>
        $forall Entity uid user <- xs
          $if uIsAdmin || (not $ userIsLocked user)
              <tr :userIsAdmin user:.admin :userIsLocked user:.locked>
                <td .name>
                  #{userName user}
                <td .ident>
                  #{userIdent user}
                <td .phone>
                  $maybe p <- userPhone user
                    #{p}
                  $nothing
                    (vide)
                <td .carPermit>
                  _{userHasCarPermit user}
                <td .actions .text-nowrap>
                  $if uIsAdmin
                    <a href=@{UserSummaryR uid} .btn .btn-default>
                      Historique
                    <a href=@{UserEditR uid} .btn .btn-default>
                      Éditer
  |]


handleUserEditR :: UserId -> Handler Html
handleUserEditR uid = do
  user <- runDB $ get404 uid

  ((fRes, fWid), fEnc) <- runFormPost . renderDivs $ User
    <$> areq textField "Identifiant" (Just $ userIdent user)
    <*> pure (userPassword user)
    <*> pure (userVerkey user)
    <*> areq selectBoolField "Vérifié" (Just $ userVerified user)
    <*> areq textField "Nom" (Just $ userName user)
    <*> aopt textField "Email" (Just $ userEmail user)
    <*> aopt textField "Téléphone" (Just $ userPhone user)
    <*> areq selectBoolField "Permis voiture" (Just $ userHasCarPermit user)
    <*> areq selectBoolField "Admin" (Just $ userIsAdmin user)
    <*> areq selectBoolField "Bloqué" (Just $ userIsLocked user)

  case fRes of
       FormSuccess user' -> do
         updateUserLdap user'
         runDB $ replace uid user'
         setMessageI MsgUserEdited
         redirect UserR
       _ ->
         return ()

  defaultLayout $ [whamlet|
    <h1>Éditer l'utilisateur #{userName user}

    <form method=post enctype="#{fEnc}">
      ^{fWid}
      <button type=submit .btn .btn-primary>
        Éditer l'utilisateur
  |]

