{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Home where

import Import
import qualified Database.Esqueleto as E
import qualified Prelude as Unsafe (head)

-- This is a handler function for the GET request method on the HomeR
-- resource pattern. All of your resource patterns are defined in
-- config/routes
--
-- The majority of the code you will write in Yesod lives in these handler
-- functions. You can spread them across multiple files if you are so
-- inclined, or create a single monolithic file.
handleHomeR :: Handler Html
handleHomeR = do
    Entity uId _ <- requireAuth
    time <- liftIO $ getCurrentTime

    mLastClosed <- runDB $ selectFirst [] [Desc ClosedTime]

    ((fRes, fWidget), fEnc) <- runFormPost $ commandForm uId
    case fRes of
      FormSuccess qts -> do
        let qts' = filter ((/= 0.0) . snd) qts
        qtsPriced <- forM qts' $ \(pId, qty) -> do
            price <- productPrice <$> (runDB . get404 $ pId)
            return (pId, qty, qty * price)
        let vals = for qtsPriced $ \(pId, qty, tPrice) ->
                Command uId pId qty tPrice Nothing time
        let totalPrice = sum $ for qtsPriced $ \(_, _, p) -> p
        runDB $ deleteWhere [CommandBuyer ==. uId, CommandClosedBy ==. Nothing]
        void . runDB . insertMany $ vals
        case length qts' of
            0 -> setMessageI $ MsgCommandEmpty
            n -> setMessageI $ MsgCommandComplete n totalPrice
        redirect SummaryR

      FormFailure _ -> setMessageI $ MsgCommandFail
      _ -> return ()

    mCmdStatus <- runDB $ getBy $ UniqueSetting CommandStatus

    defaultLayout $ do
        $(widgetFile "homepage")

getSummaryR :: Handler Html
getSummaryR = requireAuthId >>= getSummaryHelper

getUserSummaryR :: UserId -> Handler Html
getUserSummaryR = getSummaryHelper

getSummaryHelper :: UserId -> Handler Html
getSummaryHelper uId = do
    user <- runDB $ get404 uId

    xs <- runDB $ E.select $ E.from $ \((c `E.InnerJoin` p) `E.LeftOuterJoin` l) -> do
      E.on $ c E.^. CommandClosedBy  E.==. l E.?. ClosedId -- important ordering
      E.on $ c E.^. CommandProduct  E.==. p E.^. ProductId
      E.where_ $ c E.^. CommandBuyer E.==. E.val uId
      E.orderBy [E.desc $ c E.^. CommandTime]
      return ((c, l), p)
    let _ = xs :: [((Entity Command, Maybe (Entity Closed)), Entity Product)]

    let pxs :: [(Maybe (Entity Closed), [(Entity Command, Entity Product)])]
        pxs = sortBy (\x y -> fst x `cmpMaybeClosed` fst y) $
          map (id *** map (fst *** id)) $
          (snd . fst) `bucketOn` xs
        pxsPrices = for pxs $ \(_, xs') ->
          sum $ for xs' $ \(Entity _ c, _) -> commandTotalPrice c

    defaultLayout $(widgetFile "summary")

summaryTable :: [(Entity Command, Entity Product)] -> Bool -> Widget
summaryTable xs canModify = do
    $(widgetFile "summary_table")

postCommandLineDelete :: CommandId -> Handler Html
postCommandLineDelete cId = do
    _ <- requireAuthId
    cmd <- runDB $ get404 cId
    when (isJust $ commandClosedBy cmd) $ do
        setMessageI MsgCommandAlreadyClosed
        redirect SummaryR

    runDB $ deleteWhere [CommandId ==. cId]
    setMessageI MsgCommandProductDeleted
    redirect SummaryR

{- Helpers -}
commandForm :: UserId -> Html
    -> MForm Handler (FormResult [(ProductId, Double)], Widget)
commandForm uid fragment = do
    prods <- lift . runDB $ selectList [ProductShown ==. True] [Asc ProductName]
    let _ = prods :: [Entity Product]

    cmdProds <- lift $ map (\(Entity _ e) ->
        (commandProduct &&& commandQuantity) e) <$>
            (runDB $ selectList
                [CommandBuyer ==. uid, CommandClosedBy ==. Nothing]
                [])

    let pField p = rangeField 0.0 (productMaxQuantity p) (productStepQuantity p)
        productSets p = (fieldSettingsLabel (""::Text)) {
            fsAttrs = [ ("min", textShow (0.0::Double))
                      , ("max", textShow $ productMaxQuantity p) ]
        }
    fps <- forM prods $ \e ->
            let Entity pid p = e
                qtDef = lookup pid cmdProds `mplus` Just 0.0
            in ((,) e) <$> mreq (pField p) (productSets p) qtDef

    let gfxs = (productCategory . entityVal . fst) `bucketOn` fps

    let cats = map fst gfxs

    let html = $(widgetFile "form_command")

    let resses = for fps $ \(Entity pId _, (res, _)) -> ((,) pId) <$> res

    return (sequenceA resses, html)

cmpMaybeClosed :: Maybe (Entity Closed) -> Maybe (Entity Closed) -> Ordering
cmpMaybeClosed Nothing _ = LT
cmpMaybeClosed _ Nothing = GT
cmpMaybeClosed l r = closedTime cl2 `compare` closedTime cl1
  where Just (Entity _ cl1) = l
        Just (Entity _ cl2) = r
