{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Announce where

import Import

getAnnounceR :: Handler Html
getAnnounceR = do
  xs <- runDB $ selectList [] [Desc AnnounceTime]

  defaultLayout $(widgetFile "announce")


handleAnnounceEditR :: AnnounceId -> Handler Html
handleAnnounceEditR aid = do
  ann <- runDB $ get404 aid
  uid <- requireAuthId

  ((fRes, fWid), fEnc) <- runFormPost $ announceForm uid (Just ann)
  case fRes of
       FormSuccess ann' -> do
         runDB $ replace aid ann'
         setMessageI MsgAnnounceEdited
         redirect AnnounceR
       _ ->
         return ()

  defaultLayout $ [whamlet|
    <h1>Éditer l'annonce #{announceTitle ann}

    <form method=post enctype="#{fEnc}">
      ^{fWid}
      <button type=submit .pull-right .btn .btn-primary>
        Éditer l'annonce
      <a href=@{AnnounceDeleteR aid} .pull-right .btn .btn-danger>
        Supprimer
  |]


handleAnnounceAddR :: Handler Html
handleAnnounceAddR = do
  uid <- requireAuthId

  ((fRes, fWid), fEnc) <- runFormPost $ announceForm uid Nothing
  case fRes of
       FormSuccess ann' -> do
         _ <- runDB $ insert ann'
         setMessageI MsgAnnounceAdded
         redirect AnnounceR
       _ ->
         return ()

  defaultLayout $ [whamlet|
    <h1>Ajouter une annonce

    <form method=post enctype="#{fEnc}">
      ^{fWid}
      <button type=submit .btn .btn-primary .pull-right>
        Ajouter l'annonce
  |]


handleAnnounceDeleteR :: AnnounceId -> Handler Html
handleAnnounceDeleteR aid = do
  _ <- runDB $ get404 aid
  runDB $ delete aid
  setMessageI MsgAnnounceDeleted
  redirect AnnounceR


announceForm :: UserId -> Maybe Announce -> Form Announce
announceForm uid mAnn = renderDivs $ Announce
  <$> pure uid
  <*> areq textField "Titre" (announceTitle <$> mAnn)
  <*> areq textareaField (aTextSet ("Contenu"::Text)) (announceText <$> mAnn)
  <*> areq (selectFieldList pages) "Page" (announcePage <$> mAnn)
  <*> lift (liftIO getCurrentTime)
  <*> areq selectBoolField "Affiché" (announceShown <$> mAnn)
  where
    aTextSet t = (fieldSettingsLabel t)
        { fsAttrs = [("style", "width:500px; height:162px")] }
    pages = map (id &&& id) [(minBound::SitePage)..]
