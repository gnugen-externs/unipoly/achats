{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Link where

import Import

getLinkR :: Handler Html
getLinkR = do
  xs <- runDB $ selectList [] [Asc LinkOrder]
  uIsAdmin <- isAdmin

  defaultLayout $(widgetFile "link")


handleLinkEditR :: LinkId -> Handler Html
handleLinkEditR lid = do
  link <- runDB $ get404 lid
  uid <- requireAuthId

  ((fRes, fWid), fEnc) <- runFormPost $ linkForm uid (Just link)
  case fRes of
       FormSuccess link' -> do
         runDB $ replace lid link'
         setMessageI MsgLinkEdited
         redirect LinkR
       _ ->
         return ()

  defaultLayout $ [whamlet|
    <h1>Éditer le lien #{linkName link}

    <form method=post enctype="#{fEnc}">
      ^{fWid}
      <button type=submit .pull-right .btn .btn-primary>
        Éditer le lien
      <a href=@{LinkDeleteR lid} .pull-right .btn .btn-danger>
        Supprimer
  |]


handleLinkAddR :: Handler Html
handleLinkAddR = do
  uid <- requireAuthId

  ((fRes, fWid), fEnc) <- runFormPost $ linkForm uid Nothing
  case fRes of
       FormSuccess link' -> do
         _ <- runDB $ insert link'
         setMessageI MsgLinkAdded
         redirect LinkR
       _ ->
         return ()

  defaultLayout $ [whamlet|
    <h1>Ajouter un lien

    <form method=post enctype="#{fEnc}">
      ^{fWid}
      <button type=submit .btn .btn-primary>
        Ajouter le lien
  |]


handleLinkDeleteR :: LinkId -> Handler Html
handleLinkDeleteR lid = do
  _ <- runDB $ get404 lid
  runDB $ delete lid
  setMessageI MsgLinkDeleted
  redirect LinkR


linkForm :: UserId -> Maybe Link -> Form Link
linkForm uid mLink = renderDivs $ Link
  <$> pure uid
  <*> areq textField "Nom" (linkName <$> mLink)
  <*> areq textField "Adresse" (linkUrl <$> mLink)
  <*> areq intField "Ordre" (linkOrder <$> mLink)
