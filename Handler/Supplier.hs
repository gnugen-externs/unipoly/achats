{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Supplier where

import Import

getSupplierR :: Handler Html
getSupplierR = do
  xs <- runDB $ selectList [SupplierDeleted ==. False] [Desc SupplierName]

  defaultLayout $(widgetFile "supplier")


handleSupplierEditR :: SupplierId -> Handler Html
handleSupplierEditR sid = do
  s <- runDB $ get404 sid

  ((fRes, fWid), fEnc) <- runFormPost $ supplierForm (Just s)
  case fRes of
       FormSuccess s' -> do
         runDB $ replace sid s'
         setMessageI MsgSupplierEdited
         redirect SupplierR
       _ ->
         return ()

  defaultLayout $ [whamlet|
    <h1>Éditer le fournisseur #{supplierName s}

    <form method=post enctype="#{fEnc}">
      ^{fWid}
      <button type=submit .pull-right .btn .btn-primary>
        Éditer le fournisseur
      <a href=@{SupplierDeleteR sid} .pull-right .btn .btn-danger>
        Supprimer
  |]


handleSupplierAddR :: Handler Html
handleSupplierAddR = do
  ((fRes, fWid), fEnc) <- runFormPost $ supplierForm Nothing
  case fRes of
       FormSuccess s' -> do
         _ <- runDB $ insert s'
         setMessageI MsgSupplierAdded
         redirect SupplierR
       _ ->
         return ()

  defaultLayout $ [whamlet|
    <h1>Ajouter un fournisseur

    <form method=post enctype="#{fEnc}">
      ^{fWid}
      <button type=submit .btn .btn-primary .pull-right>
        Ajouter le fournisseur
  |]


handleSupplierDeleteR :: SupplierId -> Handler Html
handleSupplierDeleteR sid = do
  _ <- runDB $ get404 sid
  runDB $ updateWhere [SupplierId ==. sid] [SupplierDeleted =. True]
  setMessageI MsgSupplierDeleted
  redirect SupplierR


supplierForm :: Maybe Supplier -> Form Supplier
supplierForm mSup = renderDivs $ Supplier
  <$> areq textField "Nom" (supplierName <$> mSup)
  <*> areq textField "Adresse" (supplierAddress <$> mSup)
  <*> pure False
